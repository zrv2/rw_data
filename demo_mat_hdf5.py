def read_mat(infile):
    """read Matlab input

    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    d = loadmat(infile)

    return d

def read_hdf5(infile):
    """read HDF5 input

    :param infile: input file (str)
    :return: mat_file_ref
    """
    import h5py

    f = h5py.File(infile)

    return f

def read_general_mat(infile):
    """Read both mat types

    :param infile: input file (str)
    :return: mat_dict"""

    try:
        from scipy.io import loadmat
        d = loadmat(infile)
        return d
    except NotImplementedError:
        import h5py
        f = h5py.File(infile)
        m = dict(f)
        return m

    # return 0

if __name__ == "__main__":
    out = read_general_mat('mat_v73.mat')
    print(out)
